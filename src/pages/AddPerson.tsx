import React, { ChangeEvent, useState } from "react";


export default function AddPerson() {
    const [person, setPerson] = useState({
        name:'',
        firstname:'',
        age:0
    });

   

    const addPerson = () => {
        fetch('http://localhost:4000/person', {
            method:'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(person)
        }).then(response => response.json())
        .then(data => console.log(data));


    }

    
    function handleChange(e: any ) {
        setPerson({
            ...person,
            [e.target.name]:e.target.value
        });
        console.log(person);
        

      }

     function handleSubmit(e: { preventDefault: () => void; }) {
        e.preventDefault(); console.log(person);

        addPerson()
    }
    return (
        <section>
            <h1>Add Person</h1>

            <form onSubmit={handleSubmit} >
                <div className="form-group">
                    <label htmlFor="firstname">Firstname</label>
                    <input type="text" className="form-control" id="firstname" name="firstname" value={person.firstname} onChange={handleChange} />

                </div>
                <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input type="text" className="form-control" id="name" name="name" value={person.name} onChange={handleChange} />
                </div>
                <div className="form-group">
                    <label htmlFor="age">Age</label>
                    <input type="number" className="form-control" id="age" name="age" value={person.age} onChange={handleChange} />
                </div>
                <button  type="submit" className="btn btn-primary">Submit</button>
            </form>

        </section>
    )
}
